const express = require('express')
const router = express.Router()
const Institution = require('../models/Institution')

const getInstitutions = async function (req, res, next) {
  try {
    const institutions = await Institution.find({}).exec()
    res.status(200).json(institutions)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getInstitutionsId = async function (req, res, next) {
  const id = req.params.id
  console.log('id ', id)

  try {
    const institution = await Institution.findById(id).exec()
    if (institution === null) {
      try {
        const institution = await Institution.find({ building: id }).exec()
        if (institution === null) {
          return res.status(404).json({
            msg: 'Institution not found'
          })
        }
        res.json(institution)
      } catch (error) {
        return res.status(404).json({
          msg: error.message
        })
      }
    } else {
      res.json(institution)
    }
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

const addInstitution = async function (req, res, next) {
  const newInstitution = new Institution({

    institutionName: req.body.institutionName,
    institution: req.body.institution
  })
  try {
    await newInstitution.save()
    res.status(201).json(newInstitution)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateInstitution = async function (req, res, next) {
  const InstitutionId = req.params.id
  try {
    const institution = await Institution.findById(InstitutionId)

    institution.institution = req.body.institution
    institution.institutionName = req.body.institutionName

    await institution.save()
    return res.status(200).json(Institution)
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
}

const deleteInstitutionId = async function (req, res, next) {
  const InstitutionId = req.params.id
  try {
    await Institution.findByIdAndDelete(InstitutionId)
    return res.status(200).send()
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
}

router.get('/', getInstitutions) // GET All Rooms
router.get('/:id', getInstitutionsId) // GET One Institution
router.post('/', addInstitution) // POST (Add) Institution
router.put('/:id', updateInstitution)
router.delete('/:id', deleteInstitutionId)

module.exports = router
