const express = require('express')
const router = express.Router()

const Approved = require('../models/ApprovedOrderManagement')

const getApproveds = async function (req, res, next) {
  try {
    const approveds = await Approved.find({}).exec()
    res.status(200).json(approveds)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getApproved = async function (req, res, next) {
  const id = req.params.id

  try {
    const approved = await Approved.findById(id).exec()
    if (approved === null) {
      return res.status(404).json({
        msg: 'Approved not found'
      })
    }
    res.json(approved)
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

const addApproveds = async function (req, res, next) {
  // console.log(req.body)
  // const newapproved = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // approveds.push(newapproved)
  // lastId++

  const newApproved = new Approved({
    // name: req.body.name,
    // price: parseFloat(req.body.price)

    name_id: req.body.name_id,
    name_build: req.body.name_build,
    name: req.body.name

  })
  try {
    await newApproved.save()
    res.status(201).json(newApproved)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateApproved = async function (req, res, next) {
  const approvedId = req.params.id
  try {
    const approved = await Approved.findById(approvedId)

    approved.name_id = req.body.name_id
    approved.name_build = req.body.name_build
    approved.name = req.body.name

    await approved.save()
    return res.status(200).json(approved)
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }

  // const approved = {
  //   id: approvedId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = approveds.findIndex(function (item) {
  //   return item.id === approvedId
  // })
  // if (index >= 0) {
  //   approveds[index] = approved
  //   res.json(approveds[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No approved id ' + req.params.id
  //   })
  // }
}

const deleteApproved = async function (req, res, next) {
  const approvedId = req.params.id
  try {
    await Approved.findByIdAndDelete(approvedId)
    return res.status(200).send()
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
  // const index = approveds.findIndex(function (item) {
  //   return item.id === approvedId
  // })
  // if (index >= 0) {
  //   approveds.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No approved id ' + req.params.id
  //   })
  // }
}

router.get('/', getApproveds) // GET All Approveds
router.get('/:id', getApproved) // GET One approved
router.post('/', addApproveds) // POST (Add) approved
router.put('/:id', updateApproved)
router.delete('/:id', deleteApproved)

module.exports = router
