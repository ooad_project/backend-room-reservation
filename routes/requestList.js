const express = require('express')
const router = express.Router()

const Request = require('../models/RequestList')

const getRequests = async function (req, res, next) {
  try {
    const requests = await Request.find({}).exec()
    res.status(200).json(requests)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRequest = async function (req, res, next) {
  const id = req.params.id

  try {
    const request = await Request.findById(id).exec()
    if (request === null) {
      return res.status(404).json({
        msg: 'Request not found'
      })
    }
    res.json(request)
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

const addRequests = async function (req, res, next) {
  // console.log(req.body)
  // const newRequest = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // requests.push(newRequest)
  // lastId++

  const newRequest = new Request({
    // name: req.body.name,
    // price: parseFloat(req.body.price)

    rooms: req.body.rooms,
    additional: req.body.additional,
    nameRequester: req.body.nameRequester,
    startDate: req.body.startDate,
    endDate: req.body.endDate,
    reason: req.body.reason,
    status: req.body.status,
    additionalOther: req.body.additionalOther
  })
  try {
    await newRequest.save()
    res.status(201).json(newRequest)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRequest = async function (req, res, next) {
  const requestId = req.params.id
  try {
    const request = await Request.findById(requestId)

    request.rooms = req.body.rooms
    request.additional = req.body.additional
    request.nameRequester = req.body.nameRequester
    request.startDate = req.body.startDate
    request.endDate = req.body.endDate
    request.reason = req.body.reason
    request.status = req.body.status
    request.additionalOther = req.body.additionalOther

    await request.save()
    return res.status(200).json(request)
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }

  // const request = {
  //   id: requestId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = requests.findIndex(function (item) {
  //   return item.id === requestId
  // })
  // if (index >= 0) {
  //   requests[index] = request
  //   res.json(requests[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Request id ' + req.params.id
  //   })
  // }
}

const deleteRequest = async function (req, res, next) {
  const requestId = req.params.id
  try {
    await Request.findByIdAndDelete(requestId)
    return res.status(200).send()
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
  // const index = requests.findIndex(function (item) {
  //   return item.id === requestId
  // })
  // if (index >= 0) {
  //   requests.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Request id ' + req.params.id
  //   })
  // }
}

router.get('/', getRequests) // GET All Requests
router.get('/:id', getRequest) // GET One Request
router.post('/', addRequests) // POST (Add) Request
router.put('/:id', updateRequest)
router.delete('/:id', deleteRequest)

module.exports = router
