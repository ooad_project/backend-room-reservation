const express = require('express')
const router = express.Router()

const Building = require('../models/BuildingManagement')

const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({}).exec()
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBuilding = async function (req, res, next) {
  const id = req.params.id

  try {
    const building = await Building.findById(id).exec()
    if (building === null) {
      return res.status(404).json({
        msg: 'Building not found'
      })
    }
    res.json(building)
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

const addBuildings = async function (req, res, next) {
  // console.log(req.body)
  // const newBuilding = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // buildings.push(newBuilding)
  // lastId++

  const newBuilding = new Building({
    // name: req.body.name,
    // price: parseFloat(req.body.price)

    codeBuild: req.body.codeBuild,
    nameBuild: req.body.nameBuild,
    branch: req.body.branch,
    amountRooms: req.body.amountRooms
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)

    building.codeBuild = req.body.codeBuild
    building.nameBuild = req.body.nameBuild
    building.branch = req.body.branch
    building.amountRooms = req.body.amountRooms
    await building.save()
    return res.status(200).json(building)
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }

  // const building = {
  //   id: buildingId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = buildings.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   buildings[index] = building
  //   res.json(buildings[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Building id ' + req.params.id
  //   })
  // }
}

const deleteBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send()
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
  // const index = buildings.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   buildings.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Building id ' + req.params.id
  //   })
  // }
}

router.get('/', getBuildings) // GET All Buildings
router.get('/:id', getBuilding) // GET One Building
router.post('/', addBuildings) // POST (Add) Building
router.put('/:id', updateBuilding)
router.delete('/:id', deleteBuilding)

module.exports = router
