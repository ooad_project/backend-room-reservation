const express = require('express')
const router = express.Router()
const Room = require('../models/Room')

const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).exec()
    res.status(200).json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRoominBuild = async function (req, res, next) {
  const id = req.params.id
  console.log('id ', id)

  try {
    const room = await Room.findById(id).exec()
    if (room === null) {
      try {
        const room = await Room.find({ building: id }).exec()
        if (room === null) {
          return res.status(404).json({
            msg: 'Room not found'
          })
        }
        res.json(room)
      } catch (error) {
        return res.status(404).json({
          msg: error.message
        })
      }
    } else {
      res.json(room)
    }
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

const addRooms = async function (req, res, next) {
  const newRoom = new Room({

    nameRoom: req.body.nameRoom,
    detail: req.body.detail,
    building: req.body.building
  })
  try {
    await newRoom.save()
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)

    room.detail = req.body.detail
    room.nameRoom = req.body.nameRoom
    room.building = req.body.building
    await room.save()
    return res.status(200).json(room)
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }

  // const room = {
  //   id: roomId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = rooms.findIndex(function (item) {
  //   return item.id === roomId
  // })
  // if (index >= 0) {
  //   rooms[index] = room
  //   res.json(rooms[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Room id ' + req.params.id
  //   })
  // }
}

const deleteRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    await Room.findByIdAndDelete(roomId)
    return res.status(200).send()
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
}

router.get('/', getRooms) // GET All Rooms
router.get('/:id', getRoominBuild)
router.post('/', addRooms) // POST (Add) Room
router.put('/:id', updateRoom)
router.delete('/:id', deleteRoom)

module.exports = router
