const express = require('express')
const router = express.Router()

const Event = require('../models/Event')

const getEvents = async function (req, res, next) {
  try {
    console.log(req.query)
    const startDate = req.query.startDate
    const endDate = req.query.endDate
    const products = await Event.find({
      $or: [{ startDate: { $gte: startDate, $lt: endDate } },
        { endDate: { $gte: startDate, $lt: endDate } }]
    }).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

router.get('/', getEvents) // GET All Events

module.exports = router
