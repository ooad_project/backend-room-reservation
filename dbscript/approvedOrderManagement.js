const mongoose = require('mongoose')
const Approved = require('../models/ApprovedOrderManagement')
mongoose.connect('mongodb://localhost:27017/example')

async function clearApproved () {
  await Approved.deleteMany({})
}

async function main () {
  await clearApproved()

  const approved = Approved({
    approvedName: 'วิทยาการสารสนเทศ',
    approved: 'IF'

  })
  approved.save()
}

main().then(function () {
  console.log('Finishh')
})
