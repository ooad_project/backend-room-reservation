const mongoose = require('mongoose')
const Room = require('../models/Room')
mongoose.connect('mongodb://localhost:27017/example')

const Building = require('../models/BuildingManagement')

async function clearRoom () {
  await Room.deleteMany({})
}

async function main () {
  await clearRoom()
  const informaticsBuilding = new Building({ codeBuild: 'Informatics', nameBuild: 'Informatics', branch: 'bangsand', amountRooms: 25 })
  for (let i = 1; i <= 5; i++) {
    const room = Room({
      nameRoom: 'name room test ' + i,
      detail: 'จำนวนที่นั่ง 60 ที่นั่ง, ระบบเครื่องเสียง, โปรเจคเตอร์, ไมค์',
      building: informaticsBuilding

    })
    informaticsBuilding.rooms.push(room)
    room.save()
  }

  await informaticsBuilding.save()
}

main().then(function () {
  console.log('Finishh')
})
