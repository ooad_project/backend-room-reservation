const mongoose = require('mongoose')
const RequestList = require('../models/RequestList')
mongoose.connect('mongodb://localhost:27017/example')

async function clearRequestList () {
  await RequestList.deleteMany({})
}
async function main () {
  await clearRequestList()
  for (let i = 1; i <= 3; i++) {
    if (i === 1) {
      const requestList = RequestList({
        rooms: '6257f8fdfd31b6de33406b55', //   nameRoom: 'name room test 1',
        //   detail: 'จำนวนที่นั่ง 60 ที่นั่ง, ระบบเครื่องเสียง, โปรเจคเตอร์, ไมค์',
        //   building: '6257f8b0e974a4e335b62d01',
        //   __v: 0
        // }

        additional: ['speaker', 'projector'],
        nameRequester: 'เบญจมาศ กรจตุพรหม ' + i,
        startDate: new Date('2022-03-27 09:00'),
        endDate: new Date('2022-03-27 10:00'),
        reason: 'ไม่ต้องยุ่งได้ไหม',
        status: 'รออนุมัติ',
        additionalOther: ''
      })
      requestList.save()
    } if (i === 2) {
      const requestList = RequestList({
        rooms: '6257f8fdfd31b6de33406b55', //   nameRoom: 'name room test 1',
        //   detail: 'จำนวนที่นั่ง 60 ที่นั่ง, ระบบเครื่องเสียง, โปรเจคเตอร์, ไมค์',
        //   building: '6257f8b0e974a4e335b62d01',
        //   __v: 0
        // }

        additional: ['speaker', 'projector', 'microphone'],
        nameRequester: 'เบญจมาศ กรจตุพรหม ' + i,
        startDate: new Date('2022-03-27 11:00'),
        endDate: new Date('2022-03-27 12:00'),
        reason: 'ไม่ต้องยุ่งได้ไหม',
        status: 'รออนุมัติ',
        additionalOther: ''
      })
      requestList.save()
    } else {
      const requestList = RequestList({
        rooms: '6257f8fdfd31b6de33406b55', //   nameRoom: 'name room test 1',
        //   detail: 'จำนวนที่นั่ง 60 ที่นั่ง, ระบบเครื่องเสียง, โปรเจคเตอร์, ไมค์',
        //   building: '6257f8b0e974a4e335b62d01',
        //   __v: 0
        // }

        additional: ['speaker', 'projector', 'notebook', 'microphone'],
        nameRequester: 'เบญจมาศ กรจตุพรหม ' + i,
        startDate: new Date('2022-03-27 13:00'),
        endDate: new Date('2022-03-27 16:00'),
        reason: 'ไม่ต้องยุ่งได้ไหม',
        status: 'รออนุมัติ',
        additionalOther: ''
      })
      requestList.save()
    }
  }
}

main().then(function () {
  console.log('Finishh')
})
