const mongoose = require('mongoose')
const Building = require('../models/BuildingManagement')
mongoose.connect('mongodb://localhost:27017/example')

async function clearBuilding () {
  await Building.deleteMany({})
}
async function main () {
  await clearBuilding()
  for (let i = 1; i <= 5; i++) {
    const building = Building({
      codeBuild: 'code build test ' + i,
      nameBuild: 'name build test ' + i,
      branch: 'branch test ' + i,
      amountRooms: i
    })
    building.save()
  }
}

main().then(function () {
  console.log('Finishh')
})
