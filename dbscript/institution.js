const mongoose = require('mongoose')
const Institution = require('../models/Institution')
mongoose.connect('mongodb://localhost:27017/example')

async function clearInstitution () {
  await Institution.deleteMany({})
}

async function main () {
  await clearInstitution()

  const institution = Institution({
    institutionName: 'วิทยาการสารสนเทศ',
    institution: 'IF'

  })
  institution.save()
}

main().then(function () {
  console.log('Finishh')
})
