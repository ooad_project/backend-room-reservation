const mongoose = require('mongoose')
const Building = require('../models/Building')
mongoose.connect('mongodb://localhost:27017/WebVueJs')
async function clearBuilding () {
  await Building.deleteMany({})
}
async function main () {
  await clearBuilding()
  for (let i = 1; i <= 12; i++) {
    const building = new Building({
      name_code: 'Building_code',
      name: 'Building' + i,
      amount_room: 1
    })
    building.save()
  }
}

main().then(function () {
  console.log('Finish')
})
