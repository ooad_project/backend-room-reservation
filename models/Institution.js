const mongoose = require('mongoose')
const { Schema } = mongoose
const InstitutionSchema = Schema({
  institution: String,
  institutionName: String
  // rooms: [{ type: Schema.Types.ObjectId, ref: 'Rooms' }]

})

module.exports = mongoose.model('Institutions', InstitutionSchema)
