const mongoose = require('mongoose')
const { Schema } = mongoose
const roomsSchema = Schema({
  nameRoom: String,
  detail: String,
  // building: String
  building: { type: Schema.Types.ObjectId, ref: 'Builds' }

})

module.exports = mongoose.model('Rooms', roomsSchema)
