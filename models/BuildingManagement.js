const mongoose = require('mongoose')
const { Schema } = mongoose
const buildsSchema = Schema({
  codeBuild: String,
  nameBuild: String,
  branch: String,
  amountRooms: Number,
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Rooms' }]
})

module.exports = mongoose.model('Builds', buildsSchema)
