const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  name_code: String,
  name: String,
  amount_room: Number
})

module.exports = mongoose.model('Building', buildingSchema)
