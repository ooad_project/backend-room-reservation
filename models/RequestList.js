const mongoose = require('mongoose')
const { Schema } = mongoose
const requestListSchema = Schema({

  rooms: { },
  additional: [],
  nameRequester: String,
  startDate: Date,
  endDate: Date,
  reason: String,
  status: String,
  additionalOther: String
})

module.exports = mongoose.model('Requests', requestListSchema)
