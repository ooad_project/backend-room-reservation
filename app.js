const express = require('express')
const path = require('path')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const mongoose = require('mongoose')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
// const productsRouter = require('./routes/products')
const authRouter = require('./routes/auth')
const eventRouter = require('./routes/events')

const buildRouter = require('./routes/buildingsManagement')
const roomRouter = require('./routes/rooms')
const requestRouter = require('./routes/requestList')
const institutionRouter = require('./routes/institutions')
const approvedRouter = require('./routes/approvedOrderManagement')

const dotenv = require('dotenv')
dotenv.config()

// const { authenMiddleware, authorizeMiddleware } = require('./helpers/auth')
// const { ROLE } = require('./constant')

mongoose.connect('mongodb://localhost:27017/example')

const app = express()

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
// app.use('/users', authenMiddleware, authorizeMiddleware([ROLE.ADMIN, ROLE.LOCAL_ADMIN]), usersRouter)
// app.use('/products', authenMiddleware, authorizeMiddleware([ROLE.ADMIN, ROLE.LOCAL_ADMIN, ROLE.USER]), productsRouter)
app.use('/users', usersRouter)
// app.use('/products', productsRouter)

app.use('/auth', authRouter)
app.use('/events', eventRouter)

app.use('/institutions', institutionRouter)
app.use('/builds', buildRouter)
app.use('/rooms', roomRouter)
app.use('/requests', requestRouter)
app.use('/approveds', approvedRouter)

// app.use('/builds', authenMiddleware, authorizeMiddleware([ROLE.ADMIN, ROLE.LOCAL_ADMIN, ROLE.USER]), buildRouter)

module.exports = app
